package pl.sda.poznan.sort;

/**
 * Sortowanie przez wstawianie - analogia do ukladania kart
 * Na poczatku zakladamy, ze z lewej strony mamy zbior jednoelementowy, ktory juz jest posortowany
 * (zbior 1-elementowy zawsze jest posortowany) -> i = 1;
 * wybieramy pierwszy element ze zbioru nieposortowanego (zmienna j)
 * i probujemy wstawic do zbioru posortowanego.
 * Jezeli napotkamy mniejszy eelement to zamieniamy elementy
 */

public class InsertionSort {
    public static void sort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            // przechodzimy przez zbior posortowany (od tylu)
            // wazne! j > 0 ( a nie >=, aby uniknac IndexOutOfArrayException)
            for (int j = i; j > 0; j--) {
                if (arr[j] < arr[j - 1]) {
                    int helper = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = helper;
                }
            }
        }
    }
}
