package pl.sda.poznan.sort;

import org.junit.Test;

import static org.junit.Assert.*;

public class QuickSortTest {



    @Test
    public void sort() {

        int[] numbers = {20, 13, 6, 1, 2, 5, 6, 8, 12, 16, 17};
        int[] sortedNumbers = {1, 2, 5, 6, 6, 8, 12, 13, 16, 17, 20};
        QuickSort  quickSort = new QuickSort();
        quickSort.sort(numbers);
        assertArrayEquals(sortedNumbers, numbers);

    }

}