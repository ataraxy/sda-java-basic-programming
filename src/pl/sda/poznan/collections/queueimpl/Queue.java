package pl.sda.poznan.collections.queueimpl;

import pl.sda.poznan.collections.Collection;

/**
 * Kolejka - struktura danych, gdzie dodajemy elementy na koncu,
 * a usuwamy na poczatku.
 * Kolejka to lista FIFO - First In First Out
 * Element, ktory trafi do kolejki jako pierwszym
 * jako pierwszy bedzie obsluzony.
 * Ostatni element bedzie obsluzony na koncu.
 *
 * @param <E>
 */

public class Queue<E> implements Collection<E> {

    private int size;
    private Node<E> head;
    private Node<E> tails;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(E element) {
        return false;
    }

    @Override
    public boolean add(E element) {
        Node<E> newNode = new Node<>(element);
        if (head == null && tails == null) {
            head = newNode;
            tails = newNode;
        } else {
            if (head != null) {
                tails.setNext(newNode);
                head.setPrev(newNode);
                newNode.setNext(head.getPrev());
            }
            size++;
        }
        return false;
    }

    @Override
    public E remove() {

        return null;
    }

    @Override
    public void clear() {
        head = null;
        tails = null;
    }
}
