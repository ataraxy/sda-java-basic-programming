package pl.sda.poznan.sort.measure;

import pl.sda.poznan.sort.BubbleSort;
import pl.sda.poznan.sort.MergeSort;
import pl.sda.poznan.sort.QuickSort;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Program wypelniajacy losowymi wartosciami tablice
 * i mierzacy czasy sortowania
 * z wykorzystaniem zaimplementowanych algorytmow sortowania
 */
public class Program {
    public static int[] getRandomArray(int size) {
        int[] array = new int[size];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        return array;
    }

    public static void main(String[] args) {
        // sout -> podaj ile elementow wylosowac
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ile elementow wylosowac");
        int arraySize = scanner.nextInt();
        // utworzyc tablice o takim rozmiarze
        // wypelnic ja losowymi wartosciami
        int[] array = getRandomArray(arraySize);
        // posortowac babelkowo - wyswietlic czas
        bubbleSort(arraySize, array);

        //insertion sort

        //bucket sort

        // posortowac mergeSort - wyswietlic czas
        mergeSort(arraySize, array);
    }

    private static void mergeSort(int arraySize, int[] array) {
        System.out.println("Sortowanie przez scalanie");
        int[] toSortByMerge = Arrays.copyOf(array, arraySize);
        System.out.println("Rozpoczynam sortowanie.......");
        MergeSort mergeSort = new MergeSort();
        long startTimeInMillis = System.currentTimeMillis();
        mergeSort.sort(toSortByMerge);
        long endTimeInMillis = System.currentTimeMillis();
        long mergeSortResult = endTimeInMillis - startTimeInMillis;
        System.out.println("Zakonczono sortowanie.... Czas to: " + mergeSortResult);
    }

    private static void bubbleSort(int arraySize, int[] array) {
        System.out.println("Sortowanie babelkowe: ");
        int[] toSort = Arrays.copyOf(array, arraySize);
        System.out.println("Rozpoczynam sortowanie.......");
        long startTime = System.currentTimeMillis();
        BubbleSort.sort(toSort);
        long endTime = System.currentTimeMillis();
        long bubbleSortTime = endTime - startTime;
        System.out.println("Zakonczono sortowanie.... Czas to: " + bubbleSortTime);
    }

//    private static void quickSortExample(int arraySize, int[]array) {
//        int[] toSortByQuickSort = Arrays.copyOf(array, arraySize);
//        System.out.println("Rozpoczynam sortowanie quicksort.....");
//        long startTime = System.currentTimeMillis();
//        quicksort.sort(toSortByQuickSort);
//        long endTime = System.currentTimeMillis();
//        long quickSortTime = endTime - startTime;
//        System.out.println("Zakonczono sortowanie. Czas to: " + quickSortTime);
//    }
}
