package pl.sda.poznan.jvm;

import java.util.List;


public class Program {

    public static final String PATH_TO_FILE = "D:\\IdeaProjects\\sda\\sda-java-basic-programming\\src\\plik.txt";

    // polaczyc wszystkie napisy z listy
    // w 1 napis
    public static String concatenateStringsBadExample(List<String> list) throws InterruptedException {
        String result = null;

        for (String line : list) {
            result += line;
            Thread.sleep(100);
        }

        return result;
    }

    public static String concatenateStringsWithBuilder(List<String> list) throws InterruptedException {
        StringBuilder builder = new StringBuilder();
        for (String line : list) {
            builder.append(line);
            Thread.sleep(100);
        }
        return builder.toString();
    }

    public static void main(String[] args) throws InterruptedException {

        List<String> lines = FileOperation.readLinesFromFile(PATH_TO_FILE);
        System.out.println("Wczytano: " + lines.size() + " linii...");
        String wszystkieLinie = concatenateStringsWithBuilder(lines);
        System.out.println();

    }
}
